<?php

// Need to get this started. These drush functions will provide the ability
// to import a backup file into the system using either a site alias or 
// the current site context.

function hosting_backup_ui_drush_command() {

  $items = array();

  $items['backup-attach'] = array(
    'description' => "Attach a backup file to an Aegir site.",
    'arguments' => array(
      'search' => 'Optional argument to retrive the cartoons matching an index number, keyword search or "random". If omitted the latest cartoon will be retrieved.',
    ),
    'options' => array(
      'image-viewer' => 'Command to use to view images (e.g. xv, firefox). Defaults to "display" (from ImageMagick).',
      'google-custom-search-api-key' => 'Google Custom Search API Key, available from https://code.google.com/apis/console/. Default key limited to 100 queries/day globally.',
    ),
    'examples' => array(
      'drush xkcd' => 'Retrieve and display the latest cartoon.',
      'drush xkcd sandwich' => 'Retrieve and display cartoons about sandwiches.',
      'drush xkcd 123 --image-viewer=eog' => 'Retrieve and display cartoon #123 in eog.',
      'drush xkcd random --image-viewer=firefox' => 'Retrieve and display a random cartoon in Firefox.',
    ),
    'aliases' => array('bkat'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  $items['backup-list'] = array(
    'description' => "List all backups to an Aegir site.",
    'arguments' => array(
      'search' => ''
    ),
    'options' => array(
      'image-viewer' => 'Command to use to view images (e.g. xv, firefox). Defaults to "display" (from ImageMagick).',
      'google-custom-search-api-key' => ''
    ),
    'examples' => array(
      'drush xkcd' => 'Retrieve and display the latest cartoon.',
      'drush xkcd sandwich' => 'Retrieve and display cartoons about sandwiches.',
      'drush xkcd 123 --image-viewer=eog' => 'Retrieve and display cartoon #123 in eog.',
      'drush xkcd random --image-viewer=firefox' => 'Retrieve and display a random cartoon in Firefox.',
    ),
    'aliases' => array('bkls'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  return $items;
}

function hosting_backup_ui_drush_help($section) {
  switch ($section) {
    case 'drush:backup-attach':
      return dt("This provides the ability to attach an aegir backup file to site.");
    case 'drush:backup-list':
      return dt("This provides the ability to list all the backups attached to a given Aegir site.");
  }
}

function drush_backup_attach($search = '') {

  if (empty($search)) {
    drush_xkcd_display('http://xkcd.com');
  }
  elseif (is_numeric($search)) {
    drush_xkcd_display('http://xkcd.com/' . $search);
  }
  elseif ($search == 'random') {
    $xkcd_response = @json_decode(file_get_contents('http://xkcd.com/info.0.json'));
    if (!empty($xkcd_response->num)) {
      drush_xkcd_display('http://xkcd.com/' . rand(1, $xkcd_response->num));
    }
  }
  else {
    // This uses an API key with a limited number of searches per
    $search_response = @json_decode(file_get_contents('https://www.googleapis.com/customsearch/v1?key=' . drush_get_option('google-custom-search-api-key', 'AIzaSyDpE01VDNNT73s6CEeJRdSg5jukoG244ek') . '&cx=012652707207066138651:zudjtuwe28q&q=' . $search));
    if (!empty($search_response->items)) {
      foreach ($search_response->items as $item) {
        drush_xkcd_display($item->link);
      }
    }
    else {
      drush_set_error('DRUSH_XKCD_SEARCH_FAIL', dt('The search failed or produced no results.'));
    }
  }
}
